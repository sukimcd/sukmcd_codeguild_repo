###          phone_numbers.py          ###


"""Write a small app that allows the user to ineract with a text-based phone book by searching for, adding, changing, or removing a name and a ten-digit phone number. Users may also print the entire phone book to the screen or exit the app from the same menu."""

> Phone number? All digits. 5035551234
> 503-555-1234
> (503) 555-1234

[Source](/demos/phone-number.py)


"""Write a small app that allows the user to ineract with a text-based phone book by searching for, adding, changing, or removing a name and a ten-digit phone number. This interaction occurs through a 5-item menu, as shown below:"""

print("Welcome to the Phone Book!")

def menu(phonebook):
    phonekey = [1, 2, 3, 4, 5, 6]
    menu_op = ["search", "add", "change", "remove", "print", "exit"]
    
    for index, keyvalue in enumerate(phonekey): 
        print("Press {} to {} the phone book.".format(keyvalue, menu_op[index])


#Prompt for user input, forcing Type to String so that .index can be used
incoming = str(incoming.input("Please type the 10-digit phone number you\n  wish to add to the Phone List."))
                              