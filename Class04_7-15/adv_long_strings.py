###     adv_long_strings.py      ###

"""
# return the index of each occurrence of a specific element within an incoming argument.
>>> locate('l', 'hello')
[2, 3]

>>> locate(7, 873270)
[1, 4]

>>> locate('i', 'supercalifragilisticexpialidocious')
[8, 13, 15, 18, 23, 26, 30]

"""

"""
Develop a program which will return the index of each occurrence of a specific element within a passed in value.

Define the function "locate", assigning variable names to each element within the incoming argument
"""

def locate(des_elem, incoming_value):
    #Force incoming value to String
    incoming_value = str(incoming_value)
    
    #Define a list for the output
    occ_list = []     
    
    """Use the "Enumerate" function to locate each occurrence of desired element in incoming value, iterating over the entire incoming value until the end is reached."""
    for index, elem in enumerate(incoming_value):
        if elem == str(des_elem):
            occ_list.append(index)
        else:
            continue
        
    #Print the reulting list to the screen.
    print(occ_list)


locate('l', 'hello')
locate(7, 873270)
locate('i', 'supercalifragilisticexpialidocious')   