"""
>>> extract_domain("spf=pass (google.com: domain of jwackman@hvc.rr.com designates 2\
a00:1450:400c:c09::22d as permitted sender")
hvc.rr.com
"""


#Create a function that will extract a domain name from a header file
def extract_domain(incoming):
    #Cast the incoming text as a String
    incoming = str(incoming)
    #Locate the beginning of the desired string
    start_char = incoming.find('@')
    #Locate the end of the desired string
    end_char = incoming.find(' ', start_char)
    #isolate the values between the starting and ending characters
    domain = incoming[start_char + 1:end_char]
    #print the result to the screen
    print(domain)
    
    
extract_domain(extract_domain("spf=pass (google.com: domain of jwackman@hvc.rr.com designates 2a00:1450:400c:c09::22d as permitted sender"))    