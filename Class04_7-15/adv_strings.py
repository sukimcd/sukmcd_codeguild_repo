###     adv_strings.py      ###

"""
# return the index of each occurrence of a specific element within an incoming argument.
>>> locate('l', 'hello')
[2, 3]

>>> locate(7, 873270)
[1, 4]

Develop a program which will return the index of each occurrence of a specific element within a passed in value.
"""

"""
Define the function "locate", assigning variable names to each element within the incoming argument
"""

def locate(des_elem, incoming_value):
    #Force incoming value to String
    incoming_value = str(incoming_value)
    first_occ = incoming_value.find(str(des_elem))
    second_occ = incoming_value.find(str(des_elem), first_occ + 1)
    print([first_occ, second_occ])


locate('l', 'hello')
locate(7, 873270)
    