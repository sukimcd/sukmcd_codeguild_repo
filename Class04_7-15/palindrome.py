###          palindrome.py          ###

"""
Write a function that returns True if the input is a palindrome, or False, if it is not.

>>> palindrome('hannah')
True
>>> palindrome('racecar')
True
>>> palindrome('a man, a plan, a canal, Panama')
True
>>> palindrome("1 pound coconut.")
False
>>> palindrome(1234321)
True
"""

#First write a function that cleans up the input, removing extraneous information.
def clean_data(incoming):
    #If the input contains commas, replace them with empty strings.
    data = str(incoming.replace(",", ""))
    #Remove the whitespace from input and make the output case agnostic. 
    clean_string = data.strip().data.casefold()
    #Output the cleaned up input.
    return clean_string

"""Then write a function that returns True if the cleaned up input is a palindrome, or False, if it is not."""
def palindrome(phrase):
    #Call the previous function and apply it to the input.
    clean_string = clean_data(phrase)
    """Compare the cleaned data forward to cleaned data backward, returning True if they are exactly equivalent."""
    if clean_string == clean_string[-1]:
        True
    #If the 2 strings are not equivalent, return False
    else:
        False
    
"""

ADVANCED: TRY THIS WAY

def palindrome(incoming):
#Prompt for user input, forcing Type to String so that .index can be used.   
incoming = str(incoming.input("Please type either an English word or phrase, or a series of numbers, and the program will determine whether or not the input forms a palindrome \(a word or phrase that reads the same forward and backward\)."))
    
    #Then run user input through above functions.
""" 
    
    