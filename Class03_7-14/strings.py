"""
# Return the count of a letter in a string.
>>> count_letter('i', 'Antidisestablishmentarianism')
6

>>> count_letter('p', 'Pneumonoultramicroscopicsilicovolcanoconiosis')
2

>>> latest_letter('pneumonoultramicroscopicsilicovolcanoconiosis')
The latest letter is v.


>>> lower_case("SUPER!")
'super!'

>>> lower_case("        NANNANANANA BATMAN        ")
'nannananana batman'
"""

#Develop a program which will return the count of a specific letter within a string.
def count_letter(letter, word):
    word = word.lower()
    letter_count = word.count(letter)
    return letter_count


"""Develop a program that will output the latest letter (closest to the end of the alphabet) in an incoming word."""
def latest_letter(incoming):
    highest_letter = max(incoming)
    print("The latest letter is " highest_letter ".")


"""Develop a program that will output a lower case duplicate of an UPPER CASE input, omitting any whitespace."""
def lower_case(incoming):
    output = lower(incoming).strip(incoming)
    print(output)