"""
>>> knights()
'Knights who say Ni!'

To run the tests:

$ python -m doctest knights.py
"""

#Create a function that prints the desired phrase
def knights():
    print(\'Knights who say Ni!\')