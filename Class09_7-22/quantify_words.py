###          quantify_words.py          ###

"""
Write a function that quanitifies word occourances in a given string.

>>> quantify_words("Red touching black is a friend of Jack, red touching yellow can kill a fellow.")
a 2
black 1
can 1
fellow. 1
friend 1
is 1
jack 1
kill 1
of 1
red 2
touching 2
yellow 1


>>> quantify_words("In the end, it's concluded that the airspeed velocity of a \
(European) unladen swallow is about 24 miles per hour or 11 meters per second.")
(european) 1
11 1
24 1
a 1
about 1
airspeed 1
concluded 1
end 1
hour 1
in 1
is 1
it's 1
meters 1
miles 1
of 1
or 1
per 2
second. 1
swallow 1
that 1
the 2
unladen 1
velocity 1

"""


"""
Create a program that reports the number of times a specific element appears in an incoming string or phrase.
"""

import sys

#First write a function that cleans up the input, removing extraneous information.
def clean_data(incoming):
    #If the input contains punctuation marks, replace them  with empty strings.
    data = str(incoming).replace(",", "").replace(".", "").replace("(", "").replace(")", "").replace("\\", "")
    #Remove the whitespace from input and make the output case agnostic. 
    clean_string = data.strip().casefold()
    #Output the cleaned up input.
    return clean_string

"""Then, write a function that creates a dictionary relating each element in input to the occurrence count for same."""
def quantify_words(incoming):
    words_list = dict()
    #Call the "clean_data" function and apply it to the output.
    clean_string = clean_data(incoming)
    #Split the cleaned data into a list, dividing at the space characters.
    text_list = clean_string.split(" ")
    """Iterate through the list, one element at a time, adding each element to the dictionary and establishing or incrementing the count."""
    for wordkey in text_list:
        if wordkey not in words_list:
            words_list[wordkey] = 1
        elif wordkey in words_list:
            words_list[wordkey] += 1
        #Output the dictionary as a vertical list of tuples.
    for wordkey, count in words_list.items():
        print(wordkey, count)
    print("__________________")

incoming = "Red touching black is a friend of Jack, red touching yellow can kill a fellow."
quantify_words(incoming)

incoming = "In the end, it's concluded that the airspeed velocity of a \
(European) unladen swallow is about 24 miles per hour or 11 meters per second."
quantify_words(incoming)