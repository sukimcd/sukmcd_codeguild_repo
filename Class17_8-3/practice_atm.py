###          Practice: ATM          ###

Save your solution as `practice/atm.py`.

Write a program that functions as a simple ATM for a single account.

An account will be a class: it will have a field for the balance.

Write functions for the account class that:
* Deposit to the account.
* Check if enough funds for a withdrawal.
* Withdraw an allowed amount.
* Calculate 0.5% interest on the account.

Implement a user interface that lets a user pick each of those actions and updates the account.
After each action it will print the balance.

## Advanced
* Add a function called `get_standing` have it return a bool with whether the account has less than $1000 in it.
* Predatorily charge a transaction fee every time a withdrawal or deposit happens if the account is in bad standing.
* Save the account balance to a file after each operation.
Read that balance on startup so the balance persists across program starts.
* Add to each account class an account ID number.
* Allow the user to open more than one account.
Let them perform all of the above operations by account number.


import random


#Define a new Class which inherits its attributes from the "object" Class.
class Account(object):
    pass
    #Construct variables and values for all account parameters; include defaults for kwargs.
    def __init__(self, accthldr_first, accthldr_last, acct_type, acct_nmbr=None, acct_bal=0):
        self.accthldr_first = accthldr_first
        self.accthldr_last = accthldr_last
        self.acct_type = acct_type
        self.acct_bal = acct_bal
        #Check if acct_nmbr exists; if not, call the function to create one.
        if acct_nmbr = None:
            self.create_acct_number
        elif:
            self.acct_nmbr = acct_nmbr
            
            
    #Define representation of instance values for above Constructor.
    def __repr__(self):        
        return "{0.__class__.__name__}({0.accthldr_first}, {0.accthldr_last}, {0.acct_type}, {0.acct_nmbr}, {0.acct_bal}".format(self)
        
    
    #Define a function to create an account number for a new account.
    def create_acct_number(self, acct_type):
        
        #Populate index 0 of account number based on account type.
        if acct_type == 'savings':
            acct_type_code = '5'
        elif acct_type_ == 'checking':
            acct_type_code = '8'
        elif acct_type == 'market_checking':
            acct_type_code = '2'
        else:
            raise TypeError("You have entered an invalid account type. Please enter \"Checking\", \"Savings\", or \"Market Checking\".")
        
        #Generate a random 6-digit number to populate indices 2-7 of account number.
        core_acct_nmbr = random.randint(100000, 999999)
        
        #Populate indices 9-10 based on at which branch the account was created.
        if branch == 'Downtown':
            branch_code = '01'
        elif branch == 'Main':
            branch_code = '11'
        elif branch == 'Southeast':
            branch_code = '23'
            
        
        #Define structure of valid account number.
        new_acct_nmbr = "{}-{}-{}".format(acct_type_code, core_acct_nmbr, branch_code)
        
        """
        Assign output of "create_acct_number" function to acct_nmbr variable for this new account instance.
        """
        self.acct_nmbr = new_acct_nmbr
        #Display Account Number for new account on screen.
        return self.acct_nmbr
            