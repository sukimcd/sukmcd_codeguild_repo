###          atm_unit_tests.py          ###


import unittest


class TestAccountCreation(unittest.TestCase):
    
    #Define variables and defaults for desired account parameters.
    def SetUp(self):
        self.test_chk_acct = Account('checking', 1500)
        self.test_svgs_acct = Account('savings', 2974.58)
        
    def TearDown(self):
        pass
        
    #Define function to test whether output matches desired checking account parameters.
    def test_initial_checking_account_open(self):
        self.assertEqual(self.test_chk_acct.account_type, 'checking')
        self.assertEqual(self.test_chk_acct.balance, 1500)

    #Define function to test whether output matches desired savings account parameters.
    def test_initial_savings_account_open(self):
        self.assertEqual(self.test_svgs_acct.account_type, 'savings')
        self.assertEqual(self.test_svgs_acct.balance, 2974.58)
    
    """
    Define function to test whether account parameters passed in by reading from a CSV file are handled correctly.
    """
    def test_account_creation_from_csv_string(self):
        record = '5902692944186857,checking,151.5'
        test_account = Account.from_csv_string(record)
        self.assertEqual(test_account.account_type, 'checking')
        self.assertEqual(test_account.balance, 151.50)

    
