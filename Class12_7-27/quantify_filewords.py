            
"""
Create a program that reports the number of times a specific element appears in an incoming file.
"""

import sys


#First write a function that cleans up the input, removing extraneous information.
def clean_data(incoming):
    #If the input contains punctuation marks, replace them  with empty strings.
    data = str(incoming).replace(",", "").replace(".", "").replace("(", "").replace(")", "")
    #Remove the whitespace from input and make the output case agnostic. 
    clean_string = data.strip().casefold()
    #Output the cleaned up input.
    return clean_string



def quantify_words(incoming):
    words_list = dict()
    
    #Open the desired text file as readable and assign it to a variable.
    with open(incoming, 'r') as working_file:
        #Assign a variable to the output of a read operation.
        text = working_file.read()
        #Call the "clean_data" function and apply it to the output.
        clean_string = clean_data(text)
        #Split the cleaned data into a list, dividing at the space characters.
        text_list = clean_string.split(" ")
        """Iterate through the list, one element at a time, using the 'dict.get' function to add elements to the dictionary when found the first time and to increment the count when encountered the second through nth time."""
        for wordkey in text_list:
            words_list[wordkey] = words_list.get(wordkey, 0) + 1
        #Output the dictionary as a vertical list of tuples.
        for wordkey, count in words_list.items():
            print(wordkey, count)            
        
quantify_words("/Users/sukimcd/python_class/repository/pdxcodeguild/labs/ari/gettysburg-address.txt")