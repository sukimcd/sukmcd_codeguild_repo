###          RegExp.py           ###

"""Create a program using Regular Expressions to extract information from a text file. In this case we are searching for the string 'login' within the data file 'fake.log'."""

#Import the regular expressions library 
"""(Note that "import" is not a function which accepts a parameter inside parentheses; it's a command that accepts a library name following a space.)"""
import re

def process_file():
    #Open the desired input file
    with open('/Users/sukimcd/codeguild/ClassLabs/Class11_7-26') as log:
        text_list = log.read()
        return text_list
    
