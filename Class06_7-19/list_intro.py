###          list_intro.py          ###


"""
>>> a
'music'

>>> b
[17, 28, 42, 31, 12]

>>> spell_out(a)
m
u
s
i
c

>>> spell_out(b)
17
28
42
31
12

>>> first_and_last(b)
17
12

>>> first_and_last(a)
m
c

"""


def spell_out(incoming):
    for value in incoming:
        print(value)
        
def first_and_last(incoming):
    print(incoming[0])
    print(incoming[-1])
    

incoming = 'music'
incoming = [17, 28, 42, 31, 12]

    
    
"""
OR first_and_last(incoming) COULD BE DONE THIS WAY

def first_and_last(incoming):
    incoming.strip(l, value):
        print(value)
    incoming.strip(r, value):
        print(value)
        
""""
