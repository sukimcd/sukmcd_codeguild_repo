###         fizzbuzz.py          ###

"""
Given the length of the output of numbers from 1 - n:
If a number is divisible by 3, append "Fizz" to a list.
If a number is divisible by 5, append "Buzz" to that same list.
If a number is divisible by both 3 and 5, append "FizzBuzz" to the list.
If a number meets none of these rules, just append the string of the number.

>>> fizz_buzz(10)
['1', '2', 'Fizz', '4', 'Buzz', 'Fizz', '7', '8', 'Fizz', 'Buzz']

>>> joined_buzz(15)
'1 2 Fizz 4 Buzz Fizz 7 8 Fizz Buzz 11 Fizz 13 14 FizzBuzz'

"""


def fizz_buzz(incoming):
    output = []
    starter = range(1, x)
    for x in starter:
        if x % 3 == 0 and x % 5 == 0:
            output.append('FizzBuzz')
        elif x % 5 == 0:
            output.append('Buzz')
        elif x % 3 == 0:
            output.append('Fizz')
        else:
            output.append(x)
            
    return(output)
            
def joined_buzz(incoming):
    buzzyfizzy = fizz_buzz(incoming):
        joined_string = ' '.join(output)
        return(joined_string)
