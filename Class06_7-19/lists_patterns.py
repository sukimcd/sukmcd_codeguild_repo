###          list_patterns.py          ###


"""
>>> a
'music'
>>> b
[17, 28, 42, 31, 12]

>>> display_indexes(a)
m 0
u 1
s 2
i 3
c 4


>>> parallel(a, b)
m 17
u 28
s 42
i 31
c 12

"""

"""Create a program that will display a vertical list of tuples containing the index position of each element in an incoming value paired with the element itself."""

#Define a function to perform the desired action on the incoming value.
def display_index(incoming):
    #Create variables to contain the index position and the current element. 
    #Use the "enumerate" function to iterate through the incoming value.
    for index, elem in enumerate(incoming):
        #Print each tuple before moving to the next iteration
        print(elem, index)


incoming = 'music'
incoming = [17, 28, 42, 31, 12]


"""Create a program that will display a vertical list of tuples containing each element in an incoming value paired with the element that occupies the same position in a second incoming value."""

#Define a function to perform the desired action on the incoming value.
def parallel(incoming_a, incoming_b):
    #Create variables to contain the current elements.
    #Use the "zip" function to interate through and combine the incoming values.
        for elem_a, elem_b in zip(incoming_a, incoming_b):
        #Print each tuple before moving to the next iteration
        print(elem_a, elem_b)


incoming_a = 'music'
incoming_b = [17, 28, 42, 31, 12]
