"""
    >>> no_args_animal = Animal()
    >>> no_args_animal.is_animal
    True
    >>> no_args_animal.is_vegetable
    False
    >>> no_args_animal.is_mineral
    False
    >>> no_args_animal.number_of_legs
    0
    >>> no_args_animal.what_i_say
    True
    >>> no_args_animal.speak()
    Sorry, I don’t speak!

    >>> animal_with_args = Animal(number_of_legs=3, what_i_say='shazbat')
    >>> animal_with_args.is_animal
    True
    >>> animal_with_args.is_vegetable
    False
    >>> animal_with_args.is_mineral
    False
    >>> animal_with_args.number_of_legs
    3
    >>> animal_with_args.what_i_say
    False
    >>> animal_with_args.speak()
    shazbat

    """,
    'Dog': """

    >>> no_args_dog = Dog()
    >>> no_args_dog.is_animal
    True
    >>> no_args_dog.number_of_legs
    4
    >>> no_args_dog.speak()
    ruff
    >>> no_args_dog.kind
    'canine'
    >>> no_args_dog.action()
    /wags tail

    """,
    'Pet': """

    >>> no_args_pet = Pet()
    >>> no_args_pet.name
    True

    >>> pet_with_arg = Pet(name='Puddles')
    >>> pet_with_arg.name
    False

    """,
    'PetDog': """

    >>> no_args_pet_dog = PetDog()
    >>> no_args_pet_dog.is_animal
    True
    >>> no_args_pet_dog.number_of_legs
    4
    >>> no_args_pet_dog.speak()
    ruff
    >>> no_args_pet_dog.kind
    'canine'
    >>> no_args_pet_dog.action()
    /wags tail
    >>> no_args_pet_dog.name
    True

    >>> pet_dog_with_arg = PetDog(name='Fido')
    >>> pet_dog_with_arg.is_animal
    True
    >>> pet_dog_with_arg.number_of_legs
    4
    >>> pet_dog_with_arg.speak()
    ruff
    >>> pet_dog_with_arg.kind
    'canine'
    >>> pet_dog_with_arg.action()
    /wags tail
    >>> pet_dog_with_arg.name
    False

    """

#Define a new class named "Animal" which inherits its attributes from the "object" class.
class Animal(object):
    #Define attributes of the Class as direct values
    is_animal = True
    is_vegetable = False
    is_mineral = False
    number_of_legs = 0
    what_i_say = None
    
    def speak(self):
        """
        Define a function to print the sound the animal makes (when such has been provided via the "what_i_say" variable), and to print an error message to the screen when no sound is given.
        """
        if self.what_i_say == None:
            print("Sorry, I don’t speak!")
        else:
            print(self.what_i_say)
            
            
            
#Define a new class named "Animal" which inherits its attributes from the "object" class.
class Animal(object):
    
    #Define attributes of the Class as an initiator
    def __init__(self, is_animal=True, is_vegetable=False, is_mineral=False, number_of_legs=3, what_i_say='shazbat'):
        self.is_animal = is_animal
        self.is_vegetable = is_vegetable
        self.is_mineral = is_mineral
        self.genus = genus
        self.number_of_legs = number_of_legs
        self.what_i_say = what_i_say
    
        
    def speak(self):
        """
        Define a function to print the sound the Animal makes when such has been provided (via the "what_i_say" variable), and to print a text message to the screen when no sound is given.
        """
        if self.what_i_say = None:
            print("Sorry, I don’t speak!")
        else:
            print(animal.what_i_say)


    def output(self):
        """
        Define a function to print a list of the attributes for a given Animal, which calls the above "speak" function to define what sound each Animal makes, if any.
        """
        if self.is_animal == True:
            print("{name} is an {animal} with {legs}; it says {sound}".format(name="Pet", is_animal="animal", legs=number_of_legs, sound=self.speak)

        elif self.is_vegetable == True:
            print("{name} is a {vegetable}. It therefore has {legs} and {sound}".format(name="Pet", is_vegetable="vegetable", legs="no legs", sound="makes no noise.")

        elif self.is_mineral == True:
            print("{name} is a {mineral}. It therefore has {legs} and {sound}".format(name="Pet", is_mineral="mineral", legs="no legs", sound="makes no noise.")

        
    
        
"""
'Dog' 
"""
                  
#Define a new class named "Dog" which inherits its attributes from the "Animal" class.
class Dog(Animal):
    
    #Define as direct values all attributes of the Class that are common to all members
    genus = 'canine'
    is_animal = True
    number_of_legs = 4
    
    #Define as direct values attributes of the Class that are specific to individual instances
    breed = 'Border Collie'
    what_i_say = 'ruff'
    action = '/herds other animals'             
    
                  
    """Define a function to print the sound Dog makes when such has been provided (via the "what_i_say" variable), and to print a text message to the screen when no sound is given."""
    def speak(self):
        if self.what_i_say = None:
            print("Sorry, I don’t speak!")
        else:
            print(self.what_i_say)          
    
                  
    """
    Define a function to print the action Dog performs when such has been provided (via the "action" variable), and to display an error message on the screen when no "action" is given.
    """
    def action(self):
        if self.action = None:
            print("Sorry, I don’t do anything!")
        else:
            print(self.action)

                  
                  
                  
"""
'Pet_wout'
"""

#Define a new class named "Pet" which inherits its attributes from the "Animal" class.
class Pet(Animal):
    """
    Define as a direct value an attribute of the Class that is specific to an individual instance
    """
    name = None

    
                  
"""
'Pet_with'
"""

#Define a new class named "Pet" which inherits its attributes from the "Animal" class.
class Pet(Animal):
    """
    Use an initiator to define an attribute of the Class that is specific to an individual instance and assign a default value to that attribute.
    """
    def __init__(self, name='Puddles'):
        self.name = name
        super().__init__(*args, **kwargs)
        


"""
'PetDog_wout' 
"""


"""
Define a new class named "PetDog" which inherits at least one attribute from each of the "Dog" and "Pet" classes.
"""
class PetDog(Dog, Pet):
    
    #Define attributes of the Class that are common to all members as direct values
    is_animal = True
    genus = 'canine'
    number_of_legs = 4
    
    """
    Define attributes of the Class that are specific to an individual instance as direct values, and assign default values to those attributes.
    """
    name = None
    speak(PetDog) = 'ruff'
    action(PetDog) = /wags tail

    
                  
"""
'PetDog_with' 
"""

"""
Define a new class named "PetDog" which inherits at least one attribute from each of the "Dog" and "Pet" classes.
"""
class PetDog(Dog, Pet):
    
    #Define attributes of the Class as an initiator
    def __init__(self, breed, name, genus='canine', number_of_legs=4, what_i_say='woof'):
        self.breed = breed
        self.name = name
        super().__init__(*args, **kwargs)
        speak(PetDog) = 'ruff'
        action(PetDog) = /wags tail
                  
                   
                  
    >>> pet_dog_with_arg = PetDog(name='Fido')
    >>> pet_dog_with_arg.is_animal
    True
    >>> pet_dog_with_arg.number_of_legs
    4
    >>> pet_dog_with_arg.speak()
    ruff
    >>> pet_dog_with_arg.kind
    'canine'
    >>> pet_dog_with_arg.action()
    /wags tail
    >>> pet_dog_with_arg.name is None
    False

    """
     def __init__(self, is_animal=True, is_vegetable=False, is_mineral=False, number_of_legs=3, what_i_say='shazbat'):
        self.is_animal = is_animal
        self.is_vegetable = is_vegetable
        self.is_mineral = is_mineral
        self.genus = genus
        self.number_of_legs = number_of_legs
        self.what_i_say = what_i_say

    """