"""
# Given an input list, return it in reverse.
>>> backwards([56, 57, 58, 59, 60])
[60, 59, 58, 57, 56]


# Find the max number in a given list.  Then, change every element in the list containing the first number of the maximum to the maximum number.
>>> maximus([56, 57, 58, 59, 60])
[60, 57, 58, 59, 60]

>>> maximus([56, 67, 36, 59, 60])
[67, 67, 67, 59, 67]


# Given two lists, return True if the first item in each list is the same value.
>>> compare_first_element(['migratory', 'birds', 'fly', 'south'], ['migratory', 'monopoloy', 'mind'])
True

# Return True if the first character of the second element in the list is the same. (case insensitive)
>>> compare_second_element(['migratory', 'penguins', 'fly', 'south'], ['One', 'Pound', 'Coconut'])
True

>>> compare_second_element(['migratory', 'birds', 'fly', 'south'], ['One', 'Pound', 'Coconut'])
False


# Given two lists, return one list, with all of the items of the first list, then the second
>>> smoosh(['mama', 'llama'], ['baba', 'yaga'])
['mama', 'llama', 'baba', 'yaga']


# Use a default argument to allow the user to reverse the order!
>>> smoosh(['mama', 'llama'], ['baba', 'yaga'], reverse=True)
['yaga', 'baba', 'llama', 'mama']


"""
###          list_practice.py          ###

# Given an input list, return it in reverse.
def backwards(input_list):
    output_list = input_list[::-1]
    
return output_list


print(backwards([56, 57, 58, 59, 60]))



"""Find the max number in a given list. Then, replace every element in the input list that contains the first number of the maximum with the maximum number."""

def maximus(input_list):
    max_value = str(max(input_list))
    comparator = max_value[0]
    output_list = []
    for element in input_list:
        if comparator in element:
            output_list.append(max_value)
        else:
            output_list.append(element)
            
    return output_list

print(maximus([56, 57, 58, 59, 60]))
print(maximus([56, 67, 36, 59, 60]))



"""Given two lists, return True if the first item in each list is the same value."""
def compare_first_element(input_list1, input_list2):
    first_element1 = input_list1[0]
    first_element2 = input_list2[0]
    if first_element1 == first_element2:
        return True
    else:
        return False
    
print(compare_first_element(['migratory', 'birds', 'fly', 'south'], ['migratory', 'monopoloy', 'mind']))



""" Given two lists, return True if the first character of the second element in each list is the same. (This test should be case insensitive.) """ 
def compare_second_element(input_list1, input_list2):
    second_element1 = input_list1[1][0].casefold()
    second_element2 = input_list2[1][0].casefold()
    if second_element1 == second_element2:
        return True
    else:
        return False
    
print(compare_second_element(['migratory', 'penguins', 'fly', 'south'], ['One', 'Pound', 'Coconut']))
print(compare_second_element(['migratory', 'birds', 'fly', 'south'], ['One', 'Pound', 'Coconut']))



""" Given two lists, combine them into a single list containing all of the items in the first list, followed by all the items in the second list."""
def smoosh(input_list1, input_list2):
    output_list = input_list1 + input_list2
    
    return output_list

print(smoosh(['mama', 'llama'], ['baba', 'yaga']))

              

""" Starting with the above challenge, add a default argument to allow the user to reverse the order!"""
def smoosh(input_list1, input_list2):
    output_list =             
              