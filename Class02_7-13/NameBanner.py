###          NameBanner.py          ###

#Ask user to type in a value for name
name = input('Please enter your name -->  ')

#Determine the length of value entered
name_length = len(name)
#Create the upper and lower border of the banner
banner_h = name_length * 2

#Print the banner to the screen
print('*' * banner_h)
print('*' + " " * (banner_h // 4) + name + (' ' * (banner_h // 4)) + '*')
print('*' * banner_h)
