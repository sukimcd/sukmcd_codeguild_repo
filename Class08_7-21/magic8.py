###          magic_ball.py          ###

""" Using the 'random' library, create a function that will generate a randomized response from a list of response strings."""

import random
    
#Create a list of output strings from which the random selection will take place
response = ["Yes.", "Maybe.", "No.", "Absolutely!", "Someday soon, but not now.", "Are you out of your mind?!?", "It will happen in your lifetime.", "If you're willing to work for it.", "Trust in the Universe and it will provide.", "Let go of your fear.", "We'll never know.", "42", "Heck if I know!"]
    
#Develop a user interface
def eight_ball_game():
    play_game = input("I have a Magic 8 Ball. Would you like to play? Type 'y' or 'n': ")
    while True:
        
        if play_game.casefold() != 'y' and play_game.casefold() != 'n':
            raise TypeError ("You have entered invalid input. Please press either the \ 'y' key or the 'n' key.")
        
        elif play_game.casefold() == "y": 
            
            user_input = input("What is your question? ")
            displayed_response = random.choice(response)
            print(displayed_response)
            
            play_game = input("Play again? Type 'y' or 'n'")
        
        elif play_game.casefold() == "n":
            break

        else:    
            continue
            
eight_ball_game()            