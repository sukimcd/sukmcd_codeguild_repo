"""
1. Slice off the last digit.
That is the **check digit**, and must be retained.
1. Reverse the remaining elements in the list.
1. Multiply every other element in the reversed list by 2, starting with index 0.
1. Subtract nine from numbers over nine.
1. Sum all subtracted values.
1. Take the 1s digit of that sum.
1. If that matches the check digit, the whole card number is valid.

For example, the worked out steps would be:

1. `4  5  5  6  7  3  7  5  8  6  8  9  9  8  5  5`
1. `4  5  5  6  7  3  7  5  8  6  8  9  9  8  5`
1. `5  8  9  9  8  6  8  5  7  3  7  6  5  5  4`
1. `10 8  18 9  16 6  16 5  14 3  14 6  10 5  8`
1. `1  8  9  9  7  6  7  5  5  3  5  6  1  5  8`
1. 85
1. 5
1. Valid!


>>> validate([4, 5, 5, 6, 7, 3, 7, 5, 8, 6, 8, 9, 9, 8, 5, 5])
Valid!

>>> validate([6, 5, 1, 6, 4, 3, 7, 5, 1, 6, 4, 9, 3, 8, 5, 4])
Invalid!


"""


""" Analyze a given credit card number by applying the defined formula to determine if the number is valid."""

def validate(ccnumber):
    mult_list = []
    subt_list = []
    
    #Slice off the last digit and retain it as an assigned variable.
    check_digit = ccnumber.pop(15)
    
    #Reverse the remaining elements in the list.
    rebmuncc = lst.ccnumber[::-1]
    
    #Multiply every other element in the reversed list by 2, starting with index 0.
    number for position, number in enumerate(rebmuncc) if position % 2 == 0:
        mult_num = number * 2
        mult_list.append(mult_num)
        else mult_list.append(number)    
    
    #Subtract nine from any resulting value greater than nine.
    number for number in mult_list if number > 9:
        subt_num = number -9
        subt_list.append(subt_num)
        else subt_list.append(number)